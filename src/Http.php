<?php
/**
 * @class
 * This handles all requests.
 *
 * The query parameter opts contains JSON that unpacks to the options.
 */
class Http {

  public array $rawOpts;

  /**
   * List of log file paths to parse.
   *
   * @var array
   */
  public array $logs = [];

  /**
   * List of report names to run.
   *
   * @var array
   */
  public array $reports = [];

  public Cruncher $cruncher;

  public string $error;

  public int $httpResponseCode = 200;

  public string $domainNameForGivenIp = '';

  public function __construct(array $opts) {
    $this->cruncher = new Cruncher();
    $this->cruncher->html = TRUE;

    $_ = [];
    foreach ([
      'from' => 'from',
      'to' => 'to',
      'ip' => 'ip',
      'ua' => 'ua',

      'status-code' => 'statusCode',
      'rq' => 'request',
      'ref' => 'referer',
      'took-over' => 'tookOver',
    ] as $filterOpt => $frontEnd) {
      $value = $opts[$frontEnd] ?? NULL;
      if ($value !== NULL) {
        $_[$filterOpt] = $value;
      }
    }

    if (preg_match('/^(?:\d{1,3}\.){3}\d{1,3}$/', $_['ip'] ?? '')) {
      // Looks like a 1.2.3.4 style IP address, see if we can look up a domain.
      $domain = rtrim(shell_exec("dig +short -x $_[ip]"), ". \n\t") ?: '(no PTR)';
      if ($domain) {
        $this->domainNameForGivenIp = "$_[ip] » $domain";
      }
    }

    // print json_encode(['in' => $opts,'out' =>$_], JSON_PRETTY_PRINT); exit;
    $this->cruncher->storeFilters($_);

    $this->reports = $opts['reports'] ?? ['statusesByHour', 'topIPs', 'topReferers', 'phpTookByHour', 'topPaths'];

    if (!empty($opts['L']) && !empty($opts['N'])) {
      if (in_array($opts['L'], $this->cruncher->getLogNames())) {
        $this->logs = $this->cruncher->getLogs((string) $opts['L'], (int) $opts['N']);
      }
      else {
        $this->error = 'Invalid log name';
        $this->httpResponseCode = 400;
      }
    }

    $this->rawOpts = $opts + ['reports' => $this->reports];
  }

  public function processRequest() {
    if ($this->httpResponseCode !== 200) {
      // Given options were wrong. This exits.
      $this->errorResponse();
    }

    if (!$this->logs) {
      $this->outputHtmlPage('<p>Nothing</p>', 'nothing');
    }

        
    ob_start();
    $this->cruncher->determineParseNeeds($this->reports);
    $this->cruncher->parse($this->logs);
    $parseErrors = ob_get_clean();
    ob_start();
    $this->cruncher->report($this->reports);
    $text = ob_get_clean();
    // $text = strtr($text,[ "\n" => '<br/>']);
    $this->outputHtmlPage($parseErrors . 
      '<p>' . number_format($this->cruncher->totalMatches) . " matches</p>"
      . $text, 'ok');

  }

  protected function errorResponse() {
    http_response_code($this->httpResponseCode);
    $this->outputHtmlPage(htmlspecialchars($this->error), 'error');
  }

  protected function outputHtmlPage(string $htmlContent, string $bodyClasses) {
    $html = file_get_contents(__DIR__ . '/template.html');
    $html = strtr($html, [
      '@bodyClasses@' => $bodyClasses,
      '@body@' => $htmlContent,
      '@ipToDomain@' => htmlspecialchars($this->domainNameForGivenIp),
      // '[]; // @sites@' => json_encode($this->cruncher->getLogNames()),
      '[]; // @sites@' => json_encode($this->cruncher->getLogNames()),
      '[]; // @opts@' => json_encode($this->rawOpts),
      '@debug@' => '<pre style="opacity: 0.2;">' . htmlspecialchars(json_encode([
        'logs' => $this->logs,
        'rawOpts' => $this->rawOpts,
        'filters' => $this->cruncher->filters,
      ], JSON_PRETTY_PRINT)) . '</pre>',
    ]);
    echo $html;
    exit;
  }
}
