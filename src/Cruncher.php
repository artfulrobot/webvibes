<?php

require_once __DIR__ . '/TextGraph.php';

class Cruncher {

  public $validReports = [
    'statusesByMin', 'statusesByTenMins', 'statusesByHour', 'statusesByDay',
    'phpTookByMin', 'phpTookByTenMins', 'phpTookByHour', 'phpTookByDay',
    'responseTookByMin', 'responseTookByTenMins', 'responseTookByHour', 'responseTookByDay',
    'requestsByTime', 'requestsByWeekDay', 'requestsByTimeAndWeekDay',
    'topIPs', 'topUserAgents', 'topReferers', 'topPaths', 'topPathsFull', 'raw', 'totals',
  ];

  public $html = FALSE;

  /**
   * only public for debuggin
   */
  public $filters = [];

  /**
   * These vars hold near-raw data */
  protected $pmTots = [];
  protected $phpTook = [];
  protected $rsTook = [];
  protected $byPath = [];
  protected $byPathFull = [];
  protected $byIP = [];
  protected $byReferer = [];
  protected $byUserAgent = [];
  protected $fullPathNeeded = FALSE;
  protected $rawNeeded = FALSE;
  protected $rawMatches = [];
  public $totalMatches = 0;

  public function __construct(array $filters = []) {
    $this->storeFilters($filters);
  }

  public function getLogNames(): array {
    $names = [];
    foreach (glob(LOG_DIR . "/access-*.log") as $log) {
      preg_match('@^.*/access-([^-.]+)@', $log, $matches);
      $names[$matches[1]] = 1;
    }
    ksort($names);
    $names = array_keys($names);
    return $names;
  }

  public function storeFilters($filters) {
    if (!empty($filters['from'])) {
      $_ = strtotime($filters['from']);
      if (!$_) {
        throw new \InvalidArgumentException("Invalid From date: '{$filters['from']}' did not parse.");
      }
      $this->filters['from'] = date('Y-m-d\TH:i:s', $_);
    }
    if (!empty($filters['to'])) {
      $_ = strtotime($filters['to']);
      if (!$_) {
        throw new \InvalidArgumentException("Invalid To date: '{$filters['to']}' did not parse.");
      }
      $this->filters['to'] = date('Y-m-d\TH:i:s', $_);
    }
    if (!empty($filters['rq'])) {
      $this->filters['rq'] = "@$filters[rq]@";
    }
    if (!empty($filters['ref'])) {
      $this->filters['ref'] = "@$filters[ref]@";
    }
    if (!empty($filters['ua'])) {
      $this->filters['ua'] = "@$filters[ua]@";
    }
    if (!empty($filters['took-over'])) {
      $this->filters['took-over'] = (float) $filters['took-over'];
    }

    if (!empty($filters['status-code'])) {
      $this->filters['status-code'] = "{$filters['status-code']}";
    }
    if (!empty($filters['ip'])) {
      $this->filters['ip'] = "{$filters['ip']}";
    }
  }

  public function parse(array $logfiles) {

    $from = $this->filters['from'] ?? '';
    $to = $this->filters['to'] ?? '';
    $rq = $this->filters['rq'] ?? '';
    $ref = $this->filters['ref'] ?? '';
    $ua = $this->filters['ua'] ?? '';
    $tookOver = (float) ($this->filters['took-over'] ?? 0);
    $statusCode = $this->filters['status-code'] ?? '';
    $ipPrefix = $this->filters['ip'] ?? '';
    $ipLength = strlen($ipPrefix);
    $files = count($logfiles);
    $filesRead = 0;
    $linesMatchedFiltersGlobal = 0;

    foreach ($logfiles as $logfile) {
      $filesRead++;

      $isGzipped = (substr($logfile, -3) === '.gz');
      $logFH = $isGzipped ? gzopen($logfile, 'r') : fopen($logfile, 'r');

      $linesRead = 0;
      $nextReport = time();
      $linesMatchedFilters = 0;
      while ($row = fgets($logFH)) {
        $linesRead++;

        if (time() > $nextReport && !$this->html) {
          $this->info($this->colour("{dull}Reading file $filesRead/$files, " . number_format($linesRead, 0, '.', ',') . " lines, filtered down to " . number_format($linesMatchedFilters, 0, '.', ',')
            . ', used ' . number_format(memory_get_usage(TRUE) / 1024 / 1024, 1) . 'MB{none}'
          ));
          $nextReport = time() + 5;
        }

        $parsed = json_decode($row, TRUE);
        if (!$parsed) {
          // Maybe wrong encoding.
          $this->warning("Failed to parse row - wrong encoding?\n$row");
          continue;
        }

        // First filter by date, this should be fairly fast.
        $time = $parsed['rsTime'] ?? '2100-01-01';
        if ($from && $time < $from) {
          continue;
        }
        if ($to && $time >= $to) {
          continue;
        }
        if ($rq && !preg_match($rq, $parsed['rq'])) {
          continue;
        }
        if ($ref && !preg_match($ref, $parsed['rqRef'])) {
          continue;
        }
        if ($ua && !preg_match($ua, $parsed['rqUA'])) {
          continue;
        }
        if ($tookOver && ((float) $parsed['rsTook'] < $tookOver)) {
          continue;
        }
        if ($statusCode && !preg_match("@$statusCode@", $parsed['rsStatus'])) {
          continue;
        }
        if ($ipPrefix && substr($parsed['rqIp'], 0, $ipLength) !== $ipPrefix) {
          continue;
        }

        $linesMatchedFilters++;
        // Now extract what we want.
        // Time to the minute.
        $time = substr($time, 0, 16);
        $ip = $parsed['rqIp'] ?? '';
        if (preg_match('/^(\w+) ([^? ]+)([?][^ ]*)? HTTP/', $parsed['rq'], $matches)) {
          //print $parsed['rq']; print_r($matches);
          $method = $matches[1];
          $path = $matches[2];
          $query = $matches[3] ?? '';
        }
        else {
          $method = $path = $query = '?';
        }
        // First character of status
        $status = $parsed['rsStatus'][0];

        $phpTook = ($parsed['phpTook'] !== '-') ? (float) $parsed['phpTook'] : 0;
        $rsTook = +$parsed['rsTook'];

        if ($this->rawNeeded) {
          $this->rawMatches[] = $parsed + ['phpTookFloat' => $phpTook, 'method' => $method, 'path' => $path, 'query' => $query];
        }

        // Status per minute
        $this->pmTots[$time][$status] = ($this->pmTots[$time][$status] ?? 0) + 1;

        // PHP Processing times per minute
        if ($phpTook > 0) {
          if (!isset($this->phpTook[$time])) {
            $this->phpTook[$time] = ['min' => $phpTook, 'max' => $phpTook, 'total' => $phpTook, 'count' => 1];
          }
          else {
            $_ = &$this->phpTook[$time];
            $_['min'] = min($phpTook, $_['min']);
            $_['max'] = max($phpTook, $_['max']);
            $_['count']++;
            $_['total'] += $phpTook;
            unset($_);
          }
        }

        // Response times per minute
        if (!isset($this->rsTook[$time])) {
          $this->rsTook[$time] = ['min' => $rsTook, 'max' => $rsTook, 'total' => $rsTook, 'count' => 1];
        }
        else {
          $_ = &$this->rsTook[$time];
          $_['min'] = min($rsTook, $_['min']);
          $_['max'] = max($rsTook, $_['max']);
          $_['count']++;
          $_['total'] += $rsTook;
          unset($_);
        }

        // Requests by path
        if (!isset($this->byPath[$status]["$method $path"])) {
          $this->byPath[$status]["$method $path"] = 1;
        }
        else {
          $this->byPath[$status]["$method $path"]++;
        }

        if ($this->fullPathNeeded) {
          $this->byPathFull["$status $method $path$query"] = ($this->byPathFull["$status $method $path$query"] ?? 0) + 1;
        }

        // By IP
        $this->byIP[$ip] = ($this->byIP[$ip] ?? 0) + 1;

        $referer = $parsed['rqRef'] ?? '';
        $this->byReferer[$referer] = ($this->byReferer[$referer] ?? 0) + 1;

        $userAgent = $parsed['rqUA'] ?? '';
        $this->byUserAgent[$userAgent] = ($this->byUserAgent[$userAgent] ?? 0) + 1;
      }

      if ($isGzipped) {
        gzclose($logFH);
      }
      else {
        fclose($logFH);
      }
      $linesMatchedFiltersGlobal += $linesMatchedFilters;
    }

    // Now sort the filtered log data.
    ksort($this->pmTots);
    ksort($this->rsTook);
    ksort($this->phpTook);
    foreach ($this->byPath as $status => $paths) {
      arsort($this->byPath[$status]);
    }
    //arsort($this->byPathFull);
    arsort($this->byIP);
    arsort($this->byReferer);
    arsort($this->byUserAgent);

    $this->totalMatches = $linesMatchedFiltersGlobal;
  }

  /**
   * Data is in pmTots
   */
  public function statusesByMin() {
    $this->title("Requests per minute");
    $this->byTimeAndStatusChart($this->pmTots);
  }

  /**
   * Data is in pmTots
   */
  public function statusesByTenMins() {
    $this->title("Requests per 10 minutes");
    $stats = $this->reduceStatusesTime(15, '0');
    $this->byTimeAndStatusChart($stats, ['statusesByTenMins', 'statusesByMin']);
  }

  /**
   * Data is in pmTots
   */
  public function statusesByHour() {
    $this->title("Requests per hour");
    $stats = $this->reduceStatusesTime(13);
    $this->byTimeAndStatusChart($stats, ['statusesByHour', 'statusesByTenMins']);
  }

  /**
   * Data is in pmTots
   */
  public function statusesByDay() {
    $this->title("Requests per day");
    $stats = $this->reduceStatusesTime(10);
    $this->byTimeAndStatusChart($stats, ['statusesByDay', 'statusesByHour']);
  }

  /**
   * Reduce data to the $strlen of the timestamp and ensure a complete time series.
   */
  protected function reduceStatusesTime($strlen, $suffix = '') {
    $stats = [];
    // We only need to reduce stuff if the interval
    // is greater than 1 minute.
    if ($strlen < 16) {
      foreach ($this->pmTots as $time => $byStatus) {
        $time = substr($time, 0, $strlen) . $suffix;
        if (!isset($stats[$time])) {
          $stats[$time] = $byStatus;
        }
        else {
          foreach ($byStatus as $status => $count) {
            $stats[$time][$status] = ($stats[$time][$status] ?? 0) + $count;
          }
        }
      }
    }
    else {
      // Straight copy.
      $stats = $this->pmTots;
    }
    if (!$stats) {
      return [];
    }

    $this->fillOutStatsSeries($stats, $strlen, $suffix);
    // print json_encode(array_keys($stats), JSON_PRETTY_PRINT);
    return $stats;
  }

  protected function fillOutStatsSeries(array &$stats, int $strlen, ?string $suffix = '') {
    if (!$stats) {
      return;
    }
    $increment = [
      16 => '+ 1 minutes',
      15 => '+ 10 minutes',
      13 => '+ 1 hour',
      10 => '+ 1 day',
    ][$strlen];

    // Ensure a complete series, and add in endOfPeriod timestamp
    $allTimes = array_keys($stats);
    $start = reset($allTimes);
    $end = end($allTimes);

    $zeros = array_fill_keys(array_keys($stats[$start]), 0);
    $dt = date('Y-m-d\TH:i:00', strtotime($this->fillOutDate($start) . ' ' . $increment));
    // print "got $dt\nfrom start: $start\nincrement $increment\n"; exit;
    $time = substr($dt, 0, $strlen) . $suffix;
    while ($time < $end) {
      if (!isset($stats[$time])) {
        // print "Adding $time\n";
        $stats += [$time => $zeros];
      }
      // Advance the time
      $dt = date('Y-m-d\TH:i:00', strtotime("$dt $increment"));
      // Add the end of period timestamp
      $stats[$time]['endOfPeriod'] = $dt;

      $time = substr($dt, 0, $strlen) . $suffix;
    }

    ksort($stats);
  }

  /**
   * Data is in pmTots
   */
  public function responseTookByMin() {
    $this->title("Response time per minute");
    $this->byTimeAndCountChart($this->rsTook);
  }

  /**
   */
  public function requestsByTimeAndWeekDay() {

    $this->title("Requests by day of week and hour of day");
    $this->byTime('NH',
      fn ($formatted) =>
        ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'][((int) substr($formatted, 0, 1)) - 1]
        . " " . substr($formatted, 1)
    );
  }

  /**
   */
  public function requestsByTime() {

    $this->title("Requests by hour of day");
    $this->byTime('H',
      fn ($formatted) => $formatted);
  }

  /**
   */
  public function requestsByWeekDay() {

    $this->title("Requests by day of week");
    $this->byTime('N',
      fn ($formatted) => ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'][((int) substr($formatted, 0, 1)) - 1]
    );
  }

  /**
   */
  public function responseTookByTenMins() {

    $this->title("Response time per 10 minutes");

    $stats = $this->reduceTime($this->rsTook, 15, '0');
    $this->byTimeAndCountChart($stats, ['responseTookByTenMins' => 'responseTookByMin']);
  }

  /**
   */
  public function responseTookByHour() {

    // Need to reduce the responseTook
    $this->title("PHP time per 10 minutes");

    $stats = $this->reduceTime($this->rsTook, 13);
    $this->byTimeAndCountChart($stats, ['responseTookByHour', 'responseTookByTenMins']);
  }

  /**
   */
  public function responseTookByDay() {

    // Need to reduce the responseTook
    $this->title("PHP time per day");

    $stats = $this->reduceTime($this->rsTook, 10);
    $this->byTimeAndCountChart($stats, ['responseTookByDay' => 'responseTookByHour']);
  }

  protected function reduceTime($input, $strlen, $suffix = '') {
    $stats = [];
    foreach ($input as $time => $_) {
      $time = substr($time, 0, $strlen) . $suffix;
      if (!isset($stats[$time])) {
        $stats[$time] = $_;
      }
      else {
        $stats[$time]['max'] = max($stats[$time]['max'], $_['max']);
        $stats[$time]['min'] = min($stats[$time]['min'], $_['min']);
        $stats[$time]['total'] = $stats[$time]['total'] + $_['total'];
        $stats[$time]['count'] = $stats[$time]['count'] + $_['count'];
      }
    }
    $this->fillOutStatsSeries($stats, $strlen, $suffix);
    return $stats;
  }

  /**
   * Data is in pmTots
   */
  public function phpTookByMin() {
    $this->title("PHP processing time per minute");
    $this->byTimeAndCountChart($this->phpTook);
  }

  /**
   */
  public function phpTookByTenMins() {

    // Need to reduce the phpTook
    $this->title("PHP time per 10 minutes");

    $stats = $this->reduceTime($this->phpTook, 15, '0');
    $this->byTimeAndCountChart($stats, ['phpTookByTenMins' => 'phpTookByMin']);
  }

  /**
   */
  public function phpTookByHour() {
    $this->title("PHP time per hour");

    $stats = $this->reduceTime($this->phpTook, 13);
    $this->byTimeAndCountChart($stats, ['phpTookByHour', 'phpTookByTenMins']);
  }

  /**
   */
  public function phpTookByDay() {
    $this->title("PHP time per day");

    $stats = $this->reduceTime($this->phpTook, 10);
    $this->byTimeAndCountChart($stats, ['phpTookByDay', 'phpTookByHour']);
  }

  public function topIPs() {
    $n = 30;
    $this->title("Requests by IP - top $n");

    $max = strlen(number_format(reset($this->byIP)));
    foreach ($this->byIP as $ip => $count) {
      $ip = $this->addFilterLink(sprintf('%15s', $ip), ['ip' => $ip]);
      $this->info(
        $this->colour(
          "{yellow}$ip {none}" . sprintf("%{$max}s", number_format($count))
        ));
      if ($n-- === 0) {
        break;
      }
    }
  }

  public function topReferers() {
    $n = 30;
    $this->title("Requests by Referer - top $n");

    $max = strlen(number_format(reset($this->byReferer)));
    foreach ($this->byReferer as $referer => $count) {
      $referer = $this->addFilterLink($referer, ['referer' => '^' . $referer]);
      $this->info(
        $this->colour(
          sprintf("%{$max}s", number_format($count)) . " {yellow}$referer{none}"
        ));
      if ($n-- === 0) {
        break;
      }
    }
  }

  public function topUserAgents() {
    $n = 30;
    $this->title("Requests by User Agent - top $n");

    $max = strlen(number_format(reset($this->byUserAgent)));
    foreach ($this->byUserAgent as $userAgent => $count) {
      $userAgent = $this->addFilterLink($userAgent, ['ua' => '^' . preg_quote($userAgent, '@')]);
      $this->info(
        $this->colour(
          sprintf("%{$max}s", number_format($count)) . " {yellow}$userAgent{none}"
        ));
      if ($n-- === 0) {
        break;
      }
    }
  }

  public function topPaths() {

    foreach ($this->byPath as $status => $paths) {

      $this->title("Top {$status}xx requests");

      $max = 0;
      $n = 30;
      foreach ($paths as $path => $count) {
        $max = max($max, strlen($path));
        if ($n-- === 0) {
          break;
        }
      }
      $pathWidth = "%{$max}s";

      $max = strlen(number_format(reset($paths))) + 1;
      $numberWidth = "%{$max}s";

      $n = 30;
      foreach ($paths as $path => $count) {
        $formattedPath = sprintf($pathWidth, $path);
        $formattedPath = $this->addFilterLink($formattedPath, ['request' => "^$path"]);

        $this->info(
          $this->colour(
            sprintf("{yellow}%s {none}$numberWidth", $formattedPath, number_format($count))
          ));
        if ($n-- === 0) {
          break;
        }
      }

      $this->info("");
    }
  }

  public function topPathsFull() {
    $this->title("Requests by top full paths");

    $max = 0;
    $n = 30;
    foreach ($this->byPathFull as $path => $count) {
      $max = max($max, strlen($path));
      if ($n-- === 0) {
        break;
      }
    }
    $pathWidth = "%-{$max}s";

    $max = strlen(number_format(reset($this->byPathFull))) + 1;
    $numberWidth = "%{$max}s";

    $n = 30;
    foreach ($this->byPathFull as $path => $count) {
      $formattedPath = sprintf("$pathWidth", $path);
      // full paths are weird. They are abbrreviated status code, then space, then path.
      // @fixme @todo
      $formattedPath = $this->addFilterLink($formattedPath, ['request' => '^' . substr($path, 2), 'statucCode' => '^' . substr($path, 0, 1)]);
      $this->info(
        $this->colour(
          sprintf("$numberWidth {yellow}%s{none}", number_format($count), $formattedPath)
        ));
      if ($n-- === 0) {
        break;
      }
    }
  }

  public function raw(bool $singleReport) {
    if (!$singleReport) {
      $this->title("Raw");
    }
    if ($this->html) {
      $table = '<table><thead><th>When</th><th>IP</th><th>Request</th>
        <th>status</th>
        <th>Took</th>
        <th>Size</th>
        <th>User</th>
        <th>Referer</th>
        <th>UA</th>
      </tr></thead><tbody>';
      // {
      //   "rsTime": "2022-12-16T00:05:20+00:00",
      //   "rqIp": "207.46.13.13",
      //   "rq": "GET /sites/default/files/external-images/WaterVF_0.jpg HTTP/1.1",
      //   "rsStatus": "200",
      //   "rsTook": "6.774",
      //   "phpTook": "",
      //   "rsB": "3455643",
      //   "rqUser": "",
      //   "rqRef": "",
      //   "rqUA": "Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)",
      //
      //   "method": "GET",
      //   "path": "/sites/default/files/external-images/WaterVF_0.jpg",
      //
      //   "phpTookFloat": 0,
      //   "query": ""

      $lastDay = '';
      $toggle = TRUE;
      foreach ($this->rawMatches as $row) {
        $statusColour = ['2' => '#008800', '3' => '#006688', '4' => '#aa4400', '5' => '#880000'][substr($row['rsStatus'], 0, 1)];
        if (substr($row['rsTime'], 0, 13) !== $lastDay) {
          $toggle = !$toggle;
          $lastDay = substr($row['rsTime'], 0, 13);
        }
        $unit = ['MB', 'kB', 'B'];
        $s = (int) $row['rsB'];
        while ($s > 1023 && $unit) {
          $s /= 1023;
          array_pop($unit);
        }
        $unit = end($unit) ?? 'GB';
        $sizeColour = ['GB' => '#f00', 'MB' => '#e70', 'kB' => '#000', 'B' => '#aaa'][$unit];
        $s = number_format($s, 1) . $unit;

        $t = (float) $row['rsTook'];
        if ($t < 0.2) {
          $tColour = '#aaa';
        }
        elseif ($t < 0.5) {
          $tColour = '#580';
        }
        elseif ($t < 1) {
          $tColour = '#850';
        }
        else {
          $tColour = '#800';
        }

        $table .= '<tr class="' . ($toggle ? 'stripe' : '') . '"><td>' . $row['rsTime'] . '</td>'
          . "<td>" . htmlspecialchars($row['rqIp']) . "</td>\n"
          . "<td>" . htmlspecialchars($row['rq']) . "</td>\n"
          . "<td style='color:$statusColour'>" . htmlspecialchars($row['rsStatus']) . "</td>\n"
          . "<td style='color:$tColour;'>" . htmlspecialchars($row['rsTook'] . '/' . $row['phpTook']) . "</td>\n"
          . "<td class='right' style='color:$sizeColour;'>$s</td>\n"
          . "<td>" . htmlspecialchars($row['rqUser']) . "</td>\n"
          . "<td>" . htmlspecialchars($row['rqRef']) . "</td>\n"
          . "<td>" . htmlspecialchars($row['rqUA']) . "</td>\n"
          . "</tr>\n";
      }
      $table .= "</tbody></table>";
      echo ($table);
      // erroR_log($table);
    }
    else {
      foreach ($this->rawMatches as $row) {
        $this->info(json_encode($row, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE));
      }
    }
  }

  protected function byTimeAndCountChart($stats, array $drilldownReportSwitch = []) {

    $max = 0;
    foreach ($stats as $time => $byStatus) {
      $max = max($max, $byStatus['max']);
    }

    $barWidth = 30;

    $this->info($this->colour('{bold}'
      . sprintf(
        "%-16s %-30s %5s %5s %5s %8s",
        'When',
        '',
        'Min',
        'Avg',
        'Max',
        'Requests'
      )
      . '{none}'));

    foreach ($stats as $time => $_) {
      $bar = TextGraph::create($barWidth, $max, ' ');

      // Dots up to the minimum (why?)

      // something's wrong here
      $avg = ($_['count'] > 0) ? ($_['total']/$_['count'])  : 0;

      // «=====»
      //print "$_[min] -- $avg -- $_[max] : total $_[total] / $_[count]\n";
      $bar
        ->drawBar($_['min'], '·', 0, NULL, NULL, '{red}')
        ->drawBar($_['max'], '━', $_['min'], '┣', '┫', '{cyan}', '│',
            $avg, '┿', '{green}');

      $drilldown = [
        'reportSwitch' => $drilldownReportSwitch,
        'from' => $this->fillOutDate($time),
        'to' => $stats[$time]['endOfPeriod'] ?? '',
      ];

      $dateItem = sprintf('%-16s', str_replace('T', ' ', $time));

      $this->info(sprintf(
        "%s %s %5s %5s %5s %8d",
        $this->addFilterLink($dateItem, $drilldown),
        $this->colour($bar),
        number_format($_['min'], 1),
        number_format($avg, 1),
        number_format($_['max'], 1),
        number_format($_['total']),
      ));
    }
  }

  /**
   * @var array $stats keyed by timestamp
   */
  protected function byTimeAndStatusChart($stats, $drilldownReportSwitch = []) {

    $max = 0;
    $nextTime = [];
    foreach ($stats as $time => $byStatus) {
      $max = max($max, array_sum($byStatus));
    }
    $numWidth = max(3, strlen(number_format($max))) + 1;

    $barWidth = 30;
    $barWidthWithRounding = $barWidth + 4;
    $barScale = $max ? $barWidth / $max : 0;

    $this->info($this->colour('{bold}'
      . sprintf(
        "%-16s %{$barWidth}s %{$numWidth}s %{$numWidth}s %{$numWidth}s %{$numWidth}s",
        'When',
        '',
        '2xx',
        '3xx',
        '4xx',
        '5xx'
      )
      . '{none}'));

    foreach ($stats as $time => $byStatus) {

      $cumulative = [2 => 0, 3 => 0, 4 => 0, 5 => 0];
      foreach ($cumulative as $k => $_) {
        $cumulative[$k] = ($byStatus[$k] ?? 0) + ($cumulative[$k - 1] ?? 0);
      }

      $bar = TextGraph::create($barWidth, $max, ' ')
        ->drawBar($cumulative[2] ?? 0, '━', 0, NULL, NULL, '{green}')
        ->drawBar($cumulative[2] ?? 0, '━', 0, NULL, NULL, '{green}')
        ->drawBar($cumulative[3] ?? 0, '╸', $cumulative[2], NULL, NULL, '{cyan}')
        ->drawBar($cumulative[4] ?? 0, '×', $cumulative[3], NULL, NULL, '{yellow}')
        ->drawBar($cumulative[5] ?? 0, '!', $cumulative[4], NULL, NULL, '{red}');

      $drilldown = [
        'reportSwitch' => $drilldownReportSwitch,
        'from' => $this->fillOutDate($time),
        'to' => $stats[$time]['endOfPeriod'] ?? NULL,
      ];

      $dateItem = sprintf('%-16s', str_replace('T', ' ', $time));
      $this->info(sprintf("%s %s %{$numWidth}s %{$numWidth}s %{$numWidth}s %{$numWidth}s",
        $this->addFilterLink($dateItem, $drilldown),
        $this->colour($bar),
        number_format($byStatus[2] ?? 0),
        number_format($byStatus[3] ?? 0),
        number_format($byStatus[4] ?? 0),
        number_format($byStatus[5] ?? 0),
      ), $drilldown);
    }
  }

  /**
   */
  protected function byTime($timeFormat, callable $unformat) {

    if (!$this->pmTots) return ;

    $byFormat = [];
    foreach(array_keys($this->pmTots) as $datetimeiso) {
      $f = date($timeFormat, strtotime($datetimeiso));
      $byFormat[$f] = ($byFormat[$f] ?? 0) + 1;
    }
    ksort($byFormat);

    $max = max($byFormat);
    $numWidth = max(3, strlen(number_format($max))) + 1;

    foreach ($byFormat as $when => $count) {
      $when = $unformat($when);
      $this->info(
        $this->colour(
          "{yellow}$when {none}" . sprintf("%{$numWidth}s", number_format($count))
          . " " . str_repeat('━', round($count/$max*20))
        ));
    }
  }

  /**
   * complete a date, e.g. 2000-01-02T02 -> 2000-01-02T02:00:00
   */
  protected function fillOutDate(string $dt): string {
    return $dt . substr('0000-00-00T00:00:00', strlen($dt));
  }

  protected function title($title) {
    if ($this->html) {
      $this->info('<h2>' . htmlspecialchars($title) . '</h2>');
    }
    else {
      $this->info($title);
      $this->info(str_repeat('=', mb_strlen($title) . "\n"));
    }
  }

  protected function addFilterLink(string $msg, array $filters = []): string {
    if ($this->html && $filters) {
      return '<a href="#" data-filters="'
        . htmlspecialchars(json_encode($filters)) . '">'
        . $msg . '</a>';
    }
    return $msg;
  }

  protected function info($msg) {
    if ($this->html) {
      // if $msg is wrapped in p, h* tags, don't put <br> at end.
      echo $msg . (preg_match('/^<(p|h1|h2|h3|div)\b.*>$/s', $msg) ? '' : "<br/>");
    }
    else {
      fwrite(STDOUT, "$msg\n");
    }
  }

  protected function warning($msg) {
    if ($this->html) {
      echo '<div class="warning">' . htmlspecialchars($msg) . '</div>';
    }
    else {
      fwrite(STDERR, "W: $msg\n");
    }
  }

  public function colour($msg) {
    if ($this->html) {
      return strtr($msg, [
        '{dull}'  => '<span class="dull">',
        '{green}'  => '<span class="green">',
        '{cyan}'   => '<span class="cyan">',
        '{yellow}' => '<span class="yellow">',
        '{red}'    => '<span class="red">',
        '{bold}'   => '<span class="bold">',
        '{none}'   => '</span>',
      ]);
    }
    return strtr($msg, [
      '{dull}'  => "\x1b[38;2;100;100;80m",
      '{green}'  => "\x1b[38;2;100;225;50m",
      '{cyan}'   => "\x1b[38;2;50;200;200m",
      '{yellow}' => "\x1b[38;2;200;200;50m",
      '{red}'    => "\x1b[38;2;225;50;30m",
      '{bold}'   => "\x1b[1m",
      '{none}'   => "\x1b[0m",
    ]);
  }

  public function report($reports) {
    if ($reports === ['default']) {
      $reports = ['statusesByHour', 'phpTookByHour', 'topPaths', 'topIPs', 'topReferers'];
    }
    $singleReport = count($reports) === 1;
    foreach ($reports as $report) {
      if (in_array($report, $this->validReports)) {
        $this->$report($singleReport);
        if (!$singleReport) {
          $this->info('');
        }
      }
      else {
        if (!$this->html) {
          fwrite(STDERR, "E: Invalid report name: '$report'. Try one of: " . implode(' ', $this->validReports) . "\n");
        }
        else {
          $this->warning("Invalid report name");
        }
      }
    }
  }

  public function determineParseNeeds($reports) {
    if ($reports === ['default']) {
      $reports = ['statusesByHour', 'phpTookByHour', 'topPaths', 'topIPs', 'topReferers'];
    }
    foreach ($reports as $report) {
      if (!in_array($report, $this->validReports)) {
        throw new \InvalidArgumentException("Invalid report name: '$report'. Try one of: " . implode(' ', $this->validReports) . "\n");
      }
      $this->fullPathNeeded |= $report === 'topPathsFull';
      $this->rawNeeded |= $report === 'raw';
    }
  }

  public function getLogs($pattern, $n) :array {
    $logs = [];
    foreach (glob(LOG_DIR . "/access-$pattern*") as $log) {
      $logs[$log] = filemtime($log);
    }
    arsort($logs);
    return array_keys(array_slice($logs, 0, $n));
  }

}
