<?php

class TextGraph {
  public $bar = '';
  public $chartWidthChars;
  public $scale;
  protected $colours = [];

  public static function create(int $chartWidthChars, float $maxValue, string $fillChar = ' ') {
    return new static($chartWidthChars, $maxValue, $fillChar);
  }

  public function __construct(int $chartWidthChars, float $maxValue, string $fillChar = ' ') {
    $this->bar = str_repeat($fillChar, $chartWidthChars);
    $this->colours = array_fill(0, $chartWidthChars, NULL);
    $this->chartWidthChars = $chartWidthChars;
    $this->scale = $chartWidthChars / $maxValue;
  }

  /**
   * Draw a bar representing the given value in the original domain.
   *
   * If $offsetValue given, the bar follows on from that point and the offset is removed from its value.
   */
  public function drawBar($value,
    string $fillChar = '=',
    $offsetValue = 0,
    $startChar = NULL,
    $endChar = NULL,
    $colour = NULL,
    $startEndChar = NULL,
    $mid = NULL,
    $midChar = NULL,
    $midColour = NULL
  ) {
    $x = (int) round($offsetValue * $this->scale);
    $w = (int) round(($value - $offsetValue) * $this->scale);
    if ($w < 0) {
      throw new \InvalidArgumentException("drawBar from $offsetValue to $value - value is too small!");
    }

    if ($w === 0) {
      // Don't draw bar at all if bar is 0 length, but if we have a midpoint draw that.
      if ($mid !== NULL) {
        $this->overwrite($mid, $startEndChar ?? $midChar, $midColour);
      }
      return $this;
    }

    if ($colour) {
      for ($i = $x; $i < $x + $w; $i++) {
        $this->colours[$i] = $colour;
      }
    }

    // First fill the bar with the fill char
    $this->bar = mb_substr($this->bar, 0, $x) . str_repeat($fillChar, $w) . mb_substr($this->bar, $x + $w);

    if ($w === 1 && $startEndChar) {
      $this->bar = mb_substr($this->bar, 0, $x) . $startEndChar . mb_substr($this->bar, $x + 1);
      return $this;
    }
    if ($startChar !== NULL) {
      $this->bar = mb_substr($this->bar, 0, $x) . $startChar . mb_substr($this->bar, $x + 1);
    }
    if ($endChar !== NULL) {
      $this->bar = mb_substr($this->bar, 0, $x + $w - 1) . $endChar . mb_substr($this->bar, $x + $w);
    }

    if ($mid !== NULL) {
      $this->overwrite($mid, $midChar, $midColour);
    }

    return $this;
  }

  public function overwrite($value, $text, $colour = NULL) {
    if ($value === '') {
      return $this;
    }
    $x = (int) round($value * $this->scale);
    $l = mb_strlen($text);
    //print mb_substr($this->bar, 0, $x) ."\n" . $text ."\n" . mb_substr($this->bar, $x + $l) . "\n";
    $this->bar = mb_substr($this->bar, 0, $x) . $text . mb_substr($this->bar, $x + $l);
    if ($colour) {
      for ($i = $x; $i < $x + $l; $i++) {
        $this->colours[$i] = $colour;
      }
    }
    return $this;
  }

  public function __toString() {
    $previousColour = NULL;
    $bar = '';
    foreach ($this->colours as $i => $colour) {
      if (($previousColour || $colour) && $colour !== $previousColour) {
        $bar .= '{none}' . ($colour ?? '');
        $previousColour = $colour;
      }
      $bar .= mb_substr($this->bar, $i, 1);
    }
    if ($previousColour) {
      $bar .= '{none}';
    }
    return $bar;
  }

}
