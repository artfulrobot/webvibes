document.addEventListener('DOMContentLoaded', () => {
    // This gets populated by the template engine.
    let nodes = {};
    [
        'site',
        'logCount',
        'from',
        'to',
        'reportStatus',
        'reportTimeAndDay',
        'reportPhpTook',
        'reportResponseTook',
        'topPaths',
        'topPathsFull',
        'topIPs',
        'raw',
        'topReferers',
        'topUserAgents',
        'ip',
        'statusCode',
        'referer',
        'request',
        'tookOver',
        'logCount',
        'ua',
        'submit'
    ]
        .forEach(id => { nodes[id] = document.getElementById(id); });
    // console.log({nodes});
    const listOfReports = [
          'topPaths',
          'topPathsFull',
          'topIPs',
          'topReferers',
          'topUserAgents',
          'raw'
        ];

    window.sites.forEach(site => {
        const opt = document.createElement('option');
        opt.setAttribute('value', site);
        opt.textContent = site;
        nodes.site.append(opt);
    });

    const submitForm = () => {
        // Collect data from the form.
        let d = {reports: []};
        ['site', 'from', 'to', 'ip', 'statusCode', 'referer', 'request', 'tookOver', 'logCount', 'ua'].forEach(id => {
            d[id] = nodes[id].value;
        });

        d.L = d.site;
        
        if (nodes.logCount.value) {
            d.N = nodes.logCount.value;
        }
        else if (nodes['from'].value) {
            // Figure out how many days ago that was. 
            let d2 = new Date();
            let daysAgo = 0; 
            while (d2.toISOString().substr(0, 10) > nodes['from'].value.substr(0, 10)) {
                d2.setDate(d2.getDate() - 1);
                daysAgo += 1;
            }
            d.N = daysAgo + 1;
            console.log("Figured that ", nodes.from.value, " is ", daysAgo , "days ago, so using ", d.N);
        }
        else {
            d.N = 3;
        }
        console.log("N is", d.N);

        // Collect reports.
        ['reportResponseTook', 'reportPhpTook', 'reportTimeAndDay', 'reportStatus'].forEach(selectId => {
            if (nodes[selectId].value) {
                d.reports.push(nodes[selectId].value);
            }
        });
        listOfReports.forEach(checkbox => {
            if (nodes[checkbox].checked) {
                d.reports.push(checkbox);
            }
        });

        window.location = '?opts=' + encodeURIComponent(JSON.stringify(d));
    };
    nodes.submit.addEventListener('click', e => {
        e.preventDefault();
        e.stopPropagation();
        submitForm();
    });
    [].forEach.call(document.querySelectorAll('input[type="text"]'), el => {
        el.addEventListener('keydown', e => {
            if (e.key === 'Enter') {
                e.preventDefault();
                e.stopPropagation();
                submitForm();
            }
        })
    });

    document.querySelector('.main').addEventListener('click', e => {
        if (e.originalTarget.dataset['filters']) {
            let changes = JSON.parse(e.originalTarget.dataset['filters']);
            ['site', 'from', 'to', 'request', 'referer', 'statusCode', 'ip', 'ua'].forEach(id => {
                if (changes[id]) {
                    nodes[id].value = changes[id];
                }
            });
            if (changes.reportSwitch) {
                let switchTo = changes.reportSwitch[1],
                    input = getControlForReportName(switchTo);
                if (input.tagName === 'SELECT') input.value = switchTo;
                else input.checked = true;
            }
            submitForm();
        }
        else console.log("no filters");

    });

    function getControlForReportName(reportName) {
        if (reportName.match(/^phpTookBy/)) {
            return nodes.reportPhpTook;
        }
        if (reportName.match(/requestsBy(TimeAndWeekDay|Time|WeekDay)/)) {
            return nodes.reportTimeAndDay;
        }
        else if (reportName.match(/^responseTookBy/)) {
            return nodes.responseTookBy;
        }
        else if (reportName.match(/^statusesBy/)) {
            return nodes.reportStatus;
        }
      console.log({nodes, reportName, found: nodes[reportName]});
        return nodes[reportName];
    }

    // Hydrate
    function hydrate() {
        ['site', 'from', 'to', 'request', 'referer', 'statusCode', 'ip', 'ua', 'tookOver'].forEach(id => {
            nodes[id].value = window.opts[id] || '';
        });
        // Reset selects and radios
        ['reportStatus', 'reportResponseTook', 'reportPhpTook', 'reportTimeAndDay'].forEach(select => {
            nodes[select].value = ''
        });
        window.opts.reports.forEach(reportName => {
            let ctrl = getControlForReportName(reportName);
            if (ctrl) {
                if (ctrl.tagName === 'SELECT') ctrl.value = reportName;
                else if (ctrl.tagName === 'INPUT') ctrl.checked = true;
            }
        });
        listOfReports.forEach(checkbox => {
          nodes[checkbox].checked = opts.reports.includes(checkbox);
        });
    }

    hydrate();
    document.addEventListener('visibilitychange', e => { if (document.visibilityState === 'visible') hydrate(); } );
});
