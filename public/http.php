<?php
/**
 * This is what runs all the code for http requests.
  */
require_once __DIR__ . '/../config.php';
require_once __DIR__ . '/../src/Http.php';
require_once __DIR__ . '/../src/Cruncher.php';

$request = parse_url(rawurldecode($_SERVER['REQUEST_URI']));

// Pass through css files.
if (preg_match('@^/[a-z]+\.css$@', $request['path'])) {
  $path = __DIR__ . '/../public/' . $request['path'];
  if (file_exists($path)) {
    header('Content-Type: text/css');
    readfile($path);
    exit;
  }
  http_response_code(404);
  echo "Not Found $path";
  exit;
}
elseif (preg_match('@^/[a-z]+\.js$@', $request['path'])) {
  $path = __DIR__ . '/../public/' . $request['path'];
  if (file_exists($path)) {
    header('Content-Type: text/javascript');
    readfile($path);
    exit;
  }
  http_response_code(404);
  echo "Not Found $path";
  exit;
}

$http = new Http(json_decode($_GET['opts'] ?? '{}', TRUE));
$http->processRequest();
