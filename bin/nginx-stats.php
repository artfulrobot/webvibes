#!/usr/bin/env php
<?php
/**
 *
 * Dev ideas:
 *
 * - "list IPs with more than X requests" - consumer would then loop these to do a topPaths request.
 * - "list hours with more than X Y type requests" - consumer might then request a breakdown by path, ip, minute.
 * - "list hours with slow requests" - consumer might then request a breakdown by path, ip, minute.
 *
 * Wrap all these in an 'investigate' function.
 *
 * - --to option might take a period like "+ 1 hour"; if it starts + then apply it to the From.
 */
use GetOpt\GetOpt;
use GetOpt\Option;
use GetOpt\Operand;
use GetOpt\ArgumentException;
use GetOpt\ArgumentException\Missing;

require __DIR__ . '/lib/vendor/autoload.php';


/**
 *
 *@todo replace hard-coded log file
 *{
 *"rsTime": "2021-09-04T06:59:26+01:00",
 *"rsStatus": "200",
 *"rqIp": "1.2.3.4",
 *"rq": "GET /blog/public-wont-let-dominic-cummings-whack-bbc HTTP/1.1",
 *"rqUser": "",
 *"rqRef": "https://weownit.org.uk/",
 *"rqUA": "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3427.0 Safari/537.36",
 *"rsB": "13981",
 *"rsTook": "0.072",
 *"phpTook": "0.068"
 *}
 *
 */


require_once __DIR__ . './src/Cruncher.php';

class TextGraph {
  public $bar = '';
  public $chartWidthChars;
  public $scale;
  protected $colours = [];

  public static function create(int $chartWidthChars, float $maxValue, string $fillChar = ' ') {
    return new static($chartWidthChars, $maxValue, $fillChar);
  }

  public function __construct(int $chartWidthChars, float $maxValue, string $fillChar = ' ') {
    $this->bar = str_repeat($fillChar, $chartWidthChars);
    $this->colours = array_fill(0, $chartWidthChars, NULL);
    $this->chartWidthChars = $chartWidthChars;
    $this->scale = $chartWidthChars / $maxValue;
  }

  /**
   * Draw a bar representing the given value in the original domain.
   *
   * If $offsetValue given, the bar follows on from that point and the offset is removed from its value.
   */
  public function drawBar($value,
    string $fillChar = '=',
    $offsetValue = 0,
    $startChar = NULL,
    $endChar = NULL,
    $colour = NULL,
    $startEndChar = NULL,
    $mid = NULL,
    $midChar = NULL,
    $midColour = NULL
  ) {
    $x = (int) round($offsetValue * $this->scale);
    $w = (int) round(($value - $offsetValue) * $this->scale);
    if ($w < 0) {
      throw new \InvalidArgumentException("drawBar from $offsetValue to $value - value is too small!");
    }

    if ($w === 0) {
      // Don't draw bar at all if bar is 0 length, but if we have a midpoint draw that.
      if (!empty($mid)) {
        $this->overwrite($mid, $startEndChar ?? $midChar, $midColour);
      }
      return $this;
    }

    if ($colour) {
      for ($i = $x; $i < $x + $w; $i++) {
        $this->colours[$i] = $colour;
      }
    }

    // First fill the bar with the fill char
    $this->bar = mb_substr($this->bar, 0, $x) . str_repeat($fillChar, $w) . mb_substr($this->bar, $x + $w);

    if ($w === 1 && $startEndChar) {
      $this->bar = mb_substr($this->bar, 0, $x) . $startEndChar . mb_substr($this->bar, $x + 1);
      return $this;
    }
    if ($startChar !== NULL) {
      $this->bar = mb_substr($this->bar, 0, $x) . $startChar . mb_substr($this->bar, $x + 1);
    }
    if ($endChar !== NULL) {
      $this->bar = mb_substr($this->bar, 0, $x + $w - 1) . $endChar . mb_substr($this->bar, $x + $w);
    }

    if ($mid !== NULL) {
      $this->overwrite($mid, $midChar, $midColour);
    }

    return $this;
  }

  public function overwrite($value, $text, $colour = NULL) {
    $x = (int) round($value * $this->scale);
    $l = mb_strlen($text);
    //print mb_substr($this->bar, 0, $x) ."\n" . $text ."\n" . mb_substr($this->bar, $x + $l) . "\n";
    $this->bar = mb_substr($this->bar, 0, $x) . $text . mb_substr($this->bar, $x + $l);
    if ($colour) {
      for ($i = $x; $i < $x + $l; $i++) {
        $this->colours[$i] = $colour;
      }
    }
    return $this;
  }

  public function __toString() {
    $previousColour = NULL;
    $bar = '';
    foreach ($this->colours as $i => $colour) {
      if (($previousColour || $colour) && $colour !== $previousColour) {
        $bar .= $colour ?? '{none}';
        $previousColour = $colour;
      }
      $bar .= mb_substr($this->bar, $i, 1);
    }
    if ($previousColour) {
      $bar .= '{none}';
    }
    return $bar;
  }

}

$getOpt = new GetOpt();
$x = new Cruncher();

/*
$barWidth = 80;
//print $x->colour(TextGraph::create($barWidth, 100, '_')->drawBar(10)) . "\n";
//print $x->colour(TextGraph::create($barWidth, 100, '_')->drawBar(20, '-', 10, '[', ']', '{yellow}', '|' )) . "\n";
print $x->colour(
TextGraph::create($barWidth, 100, '_')
->drawBar(80, '=', 40, '«', '»', '{red}' )
->overwrite(60, '|', '{cyan}')
) . "\n";
//print TextGraph::create(30, 100, '_')->drawBar(20, '-', 10, '[', ']', '{red}' ) . "\n";
exit;
 */


// define common options
$getOpt->addOptions([
  Option::create('h', 'help', GetOpt::NO_ARGUMENT)
    ->setDescription('Show this help and quit'),
  Option::create('f', 'from', GetOpt::REQUIRED_ARGUMENT)
    ->setArgumentName('datetime')
    ->setDescription('Filter logs since this datetime (strtotime is used).'),
  Option::create('t', 'to', GetOpt::REQUIRED_ARGUMENT)
    ->setArgumentName('datetime')
    ->setDescription('Filter logs until this datetime (strtotime is used).'),
  Option::create('r', 'rq', GetOpt::REQUIRED_ARGUMENT)
    ->setArgumentName('regex')
    ->setDescription('Filter logs whose request matches this regex. Do not add delimiters (@) is used.'),
  Option::create('R', 'ref', GetOpt::REQUIRED_ARGUMENT)
    ->setArgumentName('regex')
    ->setDescription('Filter logs whose referrer matches this regex. Do not add delimiters (@) is used.'),
  Option::create('u', 'ua', GetOpt::REQUIRED_ARGUMENT)
    ->setArgumentName('regex')
    ->setDescription('Filter logs whose User Agent matches this regex. Do not add delimiters (@) is used.'),
  Option::create('T', 'took-over', GetOpt::REQUIRED_ARGUMENT)
    ->setArgumentName('seconds')
    ->setDescription('Filter logs where the request took over this number of seconds.'),
  Option::create('s', 'status-code', GetOpt::REQUIRED_ARGUMENT)
    ->setArgumentName('statuscode')
    ->setDescription('Filter logs whose response code (200, 404 ...) matches this regex. Do not add delimiters (@) is used. Example: ^2 for all 2xx requests'),
  Option::create('i', 'ip', GetOpt::REQUIRED_ARGUMENT)
    ->setArgumentName('string-prefix')
    ->setDescription('Filter logs whose request IP this address (prefix).'),
  Option::create('l', 'logs', GetOpt::MULTIPLE_ARGUMENT)
    ->setArgumentName('/path/to/file.log')
    ->setDescription('Log file(s) to search'),
  Option::create('L', 'log-name', GetOpt::REQUIRED_ARGUMENT)
    ->setArgumentName('substring')
    ->setDescription('Will look for /var/log/nginx/access-{substring}* files, and take the latest N, see -N'),
  Option::create('N', 'log-count', GetOpt::REQUIRED_ARGUMENT)
    ->setDefaultValue(3)
    ->setArgumentName('integer')
    ->setDescription('When -L used, use the latest N of those logs'),
]);

$getOpt->addOperand(
  Operand::create('reports', \GetOpt\Operand::MULTIPLE)
    ->setDefaultValue('default')
    ->setDescription('Report(s) to generate: ' . implode(' ', $x->validReports))
);

// process arguments and catch user errors
try {
  try {
    $getOpt->process();
  }
  catch (Missing $exception) {
    // ???
    print "huh?";
    // catch missing exceptions if help is requested
    if (!$getOpt->getOption('help')) {
      throw $exception;
    }
  }
}
catch (ArgumentException $exception) {
  fwrite(STDERR, $exception->getMessage() . "\n\n" . $getOpt->getHelpText());
  exit(1);
}

if ($getOpt->getOption('help')) {
  echo $getOpt->getHelpText();
  exit;
}

// Pass filter options to our class
$_ = [];
foreach (['from', 'to', 'rq', 'ip', 'status-code', 'ref', 'ua', 'took-over'] as $filterOpt) {
  $value = $getOpt->getOption($filterOpt);
  if ($value) {
    $_[$filterOpt] = $value;
  }
}
$x->storeFilters($_);

// Pass log files.
if ($getOpt->getOption('logs') && $getOpt->getOption('log-name')) {
  fwrite(STDERR, "Don't pass --log AND --log-name\n\n" . $getOpt->getHelpText());
  exit(1);
}
if ($getOpt->getOption('logs')) {
  $logs = $getOpt->getOption('logs');
}
elseif ($getOpt->getOption('log-name')) {
  $logs = $x->getLogs($getOpt->getOption('log-name'), $getOpt->getOption('log-count'));
}
else {
  $names = $x->getLogNames();
  if ($names) {
    echo "Logs found:\n";
    foreach ($names as $i => $name) {
      print ($i + 1) . ": $name\n";
    }
    echo "Choose: ";
    $i = (int) trim(fgets(STDIN));
    if ($i === 0 || !isset($names[$i - 1])) {
      fwrite(STDERR, "nothing selected\n");
      exit(1);
    }
    echo "Show how many days' logs: (" . $getOpt->getOption('log-count') . "): ";
    $n = trim(fgets(STDIN));
    if ($n === '') {
      $n = $getOpt->getOption('log-count');
    }
    $n = (int) $n;
    if ($n === 0) {
      fwrite(STDERR, "Quit\n");
      exit(1);
    }

    $logs = $x->getLogs($names[$i - 1], $n);
  }
  else {
    fwrite(STDERR, "Need either --log OR --log-name\n\n" . $getOpt->getHelpText());
    exit(1);
  }
}

$x->determineParseNeeds($getOpt->getOperands());
$x->parse($logs);
$x->report($getOpt->getOperands());
