
# How to use.

1. SSH with local port forward:

       ssh arX -L7123:localhost:7123

2. Check this repo out on the server anywhere (e.g. ~/nobackup/).

3. Run with `bin/run`

4. Access <http://localhost:7123>


